# Ocaml Study

## Hello World

```ml
let () = Printf.printf "Hello, World!\n"
```

or 

```ml
;; Printf.printf "Hello, World!\n"
```

### Execution


```sh
$ ocaml hello.ml
```

or 


```sh
$ ocamlopt  hello.ml -o hello
$ ./hello
```

let average a b =
  (* let is not variable *)
  let sum = a +. b in
  sum /. 2.0;;

let rec range a b =
  if a > b then []
  else a :: range (a + 1) b;;

(* 2.50000 *)
Printf.printf "%f\n" (average 2.0 3.0);;

let my_ref = ref 100;;
(* 100 *)
Printf.printf "%d\n" !my_ref;;
my_ref := 50;;
(* 50 *)
Printf.printf "%d\n" !my_ref;;
